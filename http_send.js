var request = require('request');
var log = require('./logger.js');
//Send to HTTP listener
module.exports = function(src,dest,type,val) {
    
    log('KNX->HTTP -> ' + type + " -> " + src);

    var knxevent = {
        'src':src,
        'dst':dest,
        'dpt':type,
        'value':val
    };

    var srvurl = 'http://' + process.env.MEESEEKS_SRV + '/api/knx/event'; 
    request({
        url: srvurl,
        method: "POST",
        json: knxevent
    }).on('error', function(){ 
        log("api unreachable at " + srvurl,3); 
    });
};
