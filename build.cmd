docker rmi registry.gitlab.com/meeseeks/knx-bridge:latest -f

docker build -t registry.gitlab.com/meeseeks/knx-bridge:latest .
docker tag registry.gitlab.com/meeseeks/knx-bridge:latest registry.gitlab.com/meeseeks/knx-bridge:%1

docker push registry.gitlab.com/meeseeks/knx-bridge:latest
docker push registry.gitlab.com/meeseeks/knx-bridge:%1
