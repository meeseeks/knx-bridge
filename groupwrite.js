var eibd = require('eibd');
var log = require('./logger.js');

//Request a read from a eibd device
module.exports = function (opts, gad, valueToWrite,dpt, callback) {
	var conn = eibd.Connection();
	var address = eibd.str2addr(gad);

	conn.socketRemote(opts, function(err) {

		if (err) {
			log(err,4);
			return;
		}

		conn.openTGroup(address, 1, function (err) {
			if(err) {
				log(err,4);
				return;
			}

			var msg = eibd.createMessage('write', dpt, parseInt(valueToWrite));
			conn.sendAPDU(msg, function() {
				log("knx write " + valueToWrite + " -> " + gad,1);
			});
		});
	
		callback();
	});
}