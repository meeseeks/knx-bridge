#!/usr/bin/env node
'use strict';
var eibd = require('eibd');
//var request = require('request');
var http = require('http');
var url = require('url');
const querystring = require('querystring');

// Local requires
var log = require('./logger.js');
var sendToHttp = require('./http_send.js');
var sendToRedis = require('./redis_send.js');
var groupsocketlisten = require('./groupsocketlisten.js');
var groupread = require('./groupread.js');
var groupWrite = require('./groupwrite.js');
var getEnv = require('./env.js');

// Configuration
var redisHost = getEnv('REDIS_HOST','localhost');
var eibdHost = getEnv('EIBD_HOST','172.16.20.10');
var eibdPort = 6720;

var cache = {};
log("Redis listner");
var redis = require("redis")
var redisClient = redis.createClient(6379,redisHost);
var redisPublishClient = redis.createClient(6379,redisHost);
redisClient.on("error",function(err) { console.log("REDIS SUBSCRIBE CONNECTION ERROR " + err);});
redisPublishClient.on("error",function(err) { console.log("REDIS PUBLISH CONNECTION ERROR " + err);});

redisClient.on("message", function(channel, message) {
  if(channel == "knx_write_bus") {
        var knxMsg = JSON.parse(message);
        groupWrite({host: eibdHost, port: eibdPort}
            ,knxMsg.ga
            ,knxMsg.val
            ,knxMsg.dpt
            ,function () {
                log("REDIS=>KNX -> " + knxMsg.ga + " -> " + knxMsg.val,1);
            });
  }
  
});
redisClient.subscribe("knx_write_bus");

log("Starting HTTP server");

http.createServer(function(request, response) {
    var uri = url.parse(request.url).pathname; 
    
    if(uri === '/read') 
    {
        //Request a read from a Bus device
        var groupAddr = url.parse(request.url).query.split('=')[1];

        groupread({host: eibdHost, port: eibdPort}, groupAddr, function(err) {
                if(err) {
                    log(err,4);
                    return;
                } 
                
                log('Read request sent.',1);

                //Wait 100ms for the device to respond
                setTimeout(function() {
                    var cacheresp = cache[groupAddr];
                    if(typeof cacheresp === 'undefined') { return; }

                    var knxread = {
                        'addr':groupAddr,
                        'dpt':cacheresp.type,
                        'value':cacheresp.value
                    };

                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.write(JSON.stringify(knxread));
                    response.end();
                },100);
            });
    } else if(uri === '/write') {
        var qs = querystring.parse(url.parse(request.url).query);
        groupWrite({host: eibdHost, port: eibdPort},qs.addr,qs.value,qs.dpt,function () {
            response.writeHead(200, {"Content-Type": "plain/text"});
            response.write("sent");
            response.end();
        });
    } else {
        response.writeHead(400, {"Content-Type": "plain/text"});
        response.write("unkown");
        response.end();
    }
}).listen(5051);


// Listen on KNX bus  
groupsocketlisten({
      host: eibdHost,
      port: eibdPort
    }, function(err, parser) {

    if(err) { log(err,4); return;}
    log('Listening... (Ctrl-c to abort)\n',2);
    
    parser.on('write', function(src, dest, type, val){
        log('Write from '+src+' to '+dest+': '+val+' ['+type+']',1);

        var d = new Date();
        var ts = Math.round(d.getTime() / 1000);

        //sendToHttp(src, dest, type, val);
        sendToRedis(redisPublishClient,src, dest, type, val);
        cache[dest] = {
            'ts':ts,
            'type':type,
            'value':val,
            'src':src
        };
    });
        
    parser.on('response', function(src, dest, type, val) {
        log('Response from '+src+' to '+dest+': '+val+' ['+type+']',1);
        
        var d = new Date();
        var ts = Math.round(d.getTime() / 1000);
        
        //sendToHttp(src, dest, type, val);
        sendToRedis(redisPublishClient,src, dest, type, val);
        
        cache[dest] = {
            'ts':ts,
            'type':type,
            'value':val,
            'src':src
        };
    });
    
});
