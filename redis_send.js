var log = require('./logger.js');

//Send to Redis message queue
module.exports = function(redisClient,src,dest,type,val) {
    log('KNX=>REDIS -> ' + src + " -> " + dest,1);
    var knxevent = {
        'src':src,
        'dst':dest,
        'dpt':type,
        'value':val
    };
    redisClient.publish("knx_from_bus", JSON.stringify(knxevent));
};
