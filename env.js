var log = require('./logger.js');

module.exports = function (envvar, defaultValue) {
    var daValue = process.env[envvar];

    if(typeof daValue === 'undefined')
    {
        log("Using default value for ENV:" + envvar + " => " + defaultValue,3);
        daValue = defaultValue;
    }
    else {
        log("Using environment value for ENV:" + envvar + " => " + daValue,1);
    }
    return daValue;
}