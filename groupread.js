var eibd = require('eibd');

//Request a read from a eibd device
module.exports = function (opts, gad, callback) {
	var conn = new eibd.Connection();
	conn.socketRemote(opts, function(err) {

		if(err) {
			callback(err);
			return;
		}

		var address = eibd.str2addr(gad);
		conn.openTGroup(address, 0, function (err) {
			if(err) {
				callback(err);
				return;
			}
			var msg = eibd.createMessage('read');
			conn.sendAPDU(msg, callback);
		});
	});
}