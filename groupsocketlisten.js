var eibd = require('eibd');
var log = require('./logger.js');

// Opens connection to KNX bus and listens for messages
module.exports = function (opts, callback) {
    var conn = eibd.Connection();
    conn.socketRemote(opts, function(err) {
        if(err) {
            callback(err);
            return;
        }

        conn.openGroupSocket(0, function(parser) {
            callback(undefined, parser);
        });
    });

    conn.on('close', function () {
        //restart...
        log("Restarting groupsockertlisten",3);
        setTimeout(function () { groupsocketlisten(opts, callback); }, 100);
    });
};